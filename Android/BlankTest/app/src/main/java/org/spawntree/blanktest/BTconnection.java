package org.spawntree.blanktest;


import android.bluetooth.BluetoothSocket;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;


public class BTconnection extends ActionBarActivity  {

    BluetoothSocket mBTsocket=null;
    Button      sendButton;
    TextView    recivedMessages;
    ReceiveBT      dataRcv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btconnection);

        //retrive the opened socket from Global Application (get)
        mBTsocket = ((MyApplication) this.getApplication()).getSocket();

        //get the references to the button and the edit text
        sendButton = (Button) findViewById(R.id.button);
        recivedMessages = (TextView) findViewById(R.id.rcvMSG);
        recivedMessages.setHorizontallyScrolling(true);
        //crea il thread che resta in ascolto per i dati
        dataRcv = new ReceiveBT(this,mBTsocket);
       }

    @Override
    protected void onResume() {
        super.onResume();


    }
    @Override
    protected void onPause() {
        super.onPause();

        try {
            mBTsocket.close();
            //clear the socket in the GLOBAL APP VAR     (set)
            ((MyApplication) this.getApplication()).setSocket(null);

            //stop looping in Thread dataRCV
            dataRcv.stopReadThread();
          //now i can join the thread
            dataRcv.join();

            Toast.makeText(this,"BT connection closed",Toast.LENGTH_SHORT).show();

       } catch (InterruptedException e) {
           e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_btconnection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void sendMessage(View view)
    {
        if(!mBTsocket.isConnected()) {
            Toast.makeText(this, "Error: Connection Closed!", Toast.LENGTH_LONG).show();
            this.finish();
        }

        //crea il thread per mandare i dati
        new Thread(new SendBT(this,mBTsocket)).start();

    }

}