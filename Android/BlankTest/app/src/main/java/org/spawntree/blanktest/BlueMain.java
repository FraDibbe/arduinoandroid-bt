package org.spawntree.blanktest;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;


public class BlueMain extends ActionBarActivity implements Runnable {

    private final static int  REQUEST_ENABLE_BT = 2;
    public final static String  EXTRA_MESSAGE = "org.spawntree.blanktest.BlueMain";

    BluetoothAdapter mBluetoothAdapter=null;
    BluetoothSocket mBTsocket=null;
    private ArrayAdapter<String> mListAdapter =null;
    private ListView pairedList = null;
    private ArrayList<String> pairedDevices=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blue_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //construct a void arraylist of devices
        pairedDevices= new ArrayList<String>();
        //construct a void Array Adapter
        mListAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,pairedDevices);
        //find the ListView
        pairedList = (ListView)findViewById(R.id.listview);
        //set the Adapter to the List
        pairedList.setAdapter(mListAdapter);
        pairedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String clicked = (String) parent.getItemAtPosition(position);
                String macAddr = clicked.substring(clicked.indexOf("\n")+1);
                Toast.makeText(parent.getContext(), "Selected " + macAddr, Toast.LENGTH_SHORT).show();

                //activate the connection phase using the specified MAC Address
                startConnection(macAddr);
            }
        });

        //ottengo il BT l'Adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            this.finish();
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        //BT activated, so i can show the paired devices
        showPaired();

    }


    private void startConnection(String macAddress2Connect) {

        //open a BT socket connection
        BluetoothDevice mBTdevice=mBluetoothAdapter.getRemoteDevice(macAddress2Connect);
        try {
             mBTsocket = mBTdevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
               } catch (IOException e) {
            e.printStackTrace();
        }

        //LAUNCH Connection thread
         new Thread(this,"Connection").start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_blue_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_ENABLE_BT) //BT activate request is arrived
        {
            //checks if BT is activated
            if(resultCode != Activity.RESULT_OK)
            {
                Toast.makeText(this, "Can't continue to use the Ap without BT activated", Toast.LENGTH_LONG).show();
                this.finish();
            }

        }
    }


    /** this method starts the connection to a BT server
     *  +++++++ REMEBER FOR NOW THIS APP ACT ONLY AS A CLIENT +++++++++++++
     * */
    private void showPaired()
    {
        //clear all prevous paired devices
        mListAdapter.clear();

        if(mBluetoothAdapter.isEnabled()) {
            //Extract alreday paired devices
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            // If there are paired devices
            if (pairedDevices.size() > 0) {
                // Loop through paired devices
                for (BluetoothDevice device : pairedDevices) {
                    // Add the name and address to an array adapter to show in a ListView
                    mListAdapter.add(device.getName() + "\n" + device.getAddress());
                }
            }
        }
    }


    @Override //Establish a connection in a thread
    public void run() {

        String connectionStatus="";

        // Cancel discovery because it will slow down the connection, is not needed but is better to have it
    if( BluetoothAdapter.getDefaultAdapter().isDiscovering())  BluetoothAdapter.getDefaultAdapter().cancelDiscovery();

            try {
                mBTsocket.connect();
                connectionStatus="BT connection established";
                if(mBTsocket.isConnected()) Log.i("BT", "CONNECTED!");
                //save socket object to in the global variable of the application (set)
                ((MyApplication) this.getApplication()).setSocket(mBTsocket);

                //OKAY, i have a connection now: CHANGE ACTIVITY!
                //start the BT connection activity passing the MAC Address to connect with
                Intent intent = new Intent(this, BTconnection.class);

                startActivity(intent);

            } catch (IOException e) {
                connectionStatus="Error: cannot connect";
                e.printStackTrace();
            }

         //local class to display UI message from a Working thread like this one
        class UIMessageDisplay implements Runnable
        {
            String message2display;
            UIMessageDisplay(String message)
               {
                   this.message2display=message;
               }
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),message2display,Toast.LENGTH_SHORT).show();
            }
        }
        //method to run the message on the UI from a working thread
        runOnUiThread(new UIMessageDisplay(connectionStatus));



    }


}