package org.spawntree.blanktest;

import android.app.Application;
import android.bluetooth.BluetoothSocket;

/**
 * Created by Fra on 13/03/2015.
 */
public class MyApplication extends Application {

    private BluetoothSocket mySocket;

    public BluetoothSocket getSocket() {
        return mySocket;
    }

    public void setSocket(BluetoothSocket socket) {
        this.mySocket = socket;
    }
}