package org.spawntree.blanktest;

import android.app.Activity;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * Created by Fra on 13/03/2015.
 */
public class ReceiveBT extends  Thread {


    private Activity myActivity;
    private final BluetoothSocket mySocket;
    private final InputStream mmInStream;
    private boolean endLoop;

    public ReceiveBT(Activity callingAct, BluetoothSocket openSocket)
    {
        InputStream tempI=null;

        this.myActivity=callingAct;
        this.mySocket=openSocket;
        this.endLoop=false; //begin this instance in looping mode
        //retrieve the output stream
        try {
            tempI = mySocket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.mmInStream=tempI;

        this.start();
    }

    //read is a blocking call
    public void run()
    {


        byte[] buffer = new byte[2000];  // buffer store for the stream
        int bytes; // bytes returned from read()

        // Keep listening to the InputStream until an exception occurs
        while (!endLoop) {
            try {
                // Read from the InputStream
                bytes = mmInStream.read(buffer); //blocking call
                //allocate a temporary buffer of extact dimensions
                byte[] tempBuff = new byte[bytes];
                //copy the buffer content to ntemp buffer
                for(int i=0; i<bytes;i++)
                {
                    tempBuff[i]=buffer[i];
                    buffer[i]=0;
                }
                final String message = new String(tempBuff);
                Log.i("BTRCV",message);
                //show the message in UI
                myActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView recivedMessages;
                        // Send the obtained bytes to the UI activity
                        recivedMessages = (TextView) myActivity.findViewById(R.id.rcvMSG);
                        recivedMessages.append(message);
                    }
                });

            } catch (IOException e) {
                break;
            }
        }

    }

    public void stopReadThread()
    {
        this.endLoop = true;
    }


}
