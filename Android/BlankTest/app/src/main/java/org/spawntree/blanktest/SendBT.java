package org.spawntree.blanktest;

import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Fra on 12/03/2015.
 */
public class SendBT implements Runnable {

    private Activity myActivity;
    private final BluetoothSocket mySocket;
    private final OutputStream mmOutStream;

    public SendBT(Activity callingAct, BluetoothSocket openSocket)
    {

        OutputStream tempO=null;
        this.myActivity=callingAct;
        this.mySocket=openSocket;

       //retrieve the streams
        try {

            tempO = mySocket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }


        this.mmOutStream=tempO;

    }

    //this thread send the data
    public void run()
    {

        //retrive data from the activity
       final EditText sendMessage = (EditText) myActivity.findViewById(R.id.toSendMSG);
       String message=sendMessage.getText().toString();
        Log.i("BTSND", message);
        myActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sendMessage.setText("");
            }
        });
       //buffer
        byte[] byteBuffer = new byte[2000];
        byteBuffer= message.getBytes();
        byte[] tempSend = new byte[message.length()];
        //copy the buffer content to ntemp buffer
        for(int i=0; i<message.length();i++)
        {
            tempSend[i]=byteBuffer[i];
            byteBuffer[i]=0;
        }


        try {
            mmOutStream.write(tempSend);
        }
        catch (IOException e) { }

    }

}
