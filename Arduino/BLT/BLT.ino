
int const POWER = 43;
int const LED = 13;
int const KEY = 53;
int const MODE_AT =1;
int const MODE_AUTO=2;
int const LED1= 22;
int const LED2= 30;

int valLed1=LOW,valLed2=LOW;

void setup() {
  // initialize serial:
  Serial.begin(9600);
  pinMode(LED,OUTPUT);
  pinMode(KEY,OUTPUT);
  pinMode(POWER,OUTPUT);
  digitalWrite(POWER,LOW);//start the HC turned off
  
  //Test Led
  pinMode(LED1,OUTPUT);
  pinMode(LED2,OUTPUT);
  digitalWrite(LED1,LOW);//start with LED1 off
  digitalWrite(LED2,LOW);//start with LED1 off
 
 //StartTest,autonomus mode
  delay(20);
  BTstartAutonomus();
}

void loop() {
  
}

void serialEvent()
{
   byte b=Serial.read();
    Serial.write(b);
   
    switch(b) 
    {
    case '^': //ON in Autonomus Mode 9600 baud
      BTstartAutonomus();
      break;
      
    case'?': //ON in AT mode 38400 baud
     BTstartAT();
      break;
      
    case '_':
      BTdisconnect();
    break;
    
    default:
      Serial1.write(b);
      break;
      }
}

void serialEvent1()
{
    byte byteR=Serial1.read();
    commandParser(byteR);
    Serial.write(byteR);
}

/****************PARSER***********************/
void commandParser(byte command)
{
  
  switch(command)
  {
    int opp;
    case 'A':
      valLed1=oppositeVal(valLed1);
      digitalWrite(LED1,valLed1);
      Serial1.println(" LED 1");
      break;
      case 'B':
      valLed2=oppositeVal(valLed2);
      digitalWrite(LED2,valLed2);
      Serial1.println(" LED 2");
      break;
  }
}

int oppositeVal(int val)
{
  if(val == LOW) return HIGH;
  else return LOW;
}
/*****************UTILS************************/

void changeMode(int Mode)
{ 
   closeSerial1();
   
  if(Mode == MODE_AUTO)
     Serial1.begin(9600);
  else //MODE AT
     Serial1.begin(38400);
}

void closeSerial1(){
  Serial1.end();
}

//+++++++++++++++++ BT modalità funzionamento ++++++
void BTstartAutonomus(){
  Serial.println("\nAUTO");
 changeMode(MODE_AUTO);
 digitalWrite(POWER,LOW);
 digitalWrite(KEY,LOW);
 delay(50);
 digitalWrite(POWER,HIGH);
}

void BTstartAT()
{
  Serial.println("\nAT");
  changeMode(MODE_AT);
  digitalWrite(POWER,LOW);
  digitalWrite(KEY,LOW);
  delay(50);
  digitalWrite(POWER,HIGH);
  digitalWrite(KEY,HIGH);
}

void BTdisconnect()
{
  Serial.println("\nDISC");
  closeSerial1();
  digitalWrite(KEY,LOW);
  digitalWrite(POWER,LOW);
}
//+++++++++++++++++ </BT modalità funzionamento> ++++++
